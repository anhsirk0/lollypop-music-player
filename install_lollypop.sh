#!/bin/bash

sudo pacman -S pkg-config gcc cmake meson ninja gobject-introspection totem-pl-parser python-pillow

meson builddir --prefix=/usr/local
sudo ninja -C builddir install

# packages that can be removed later
# cmake
# gobject-introspection
# jsoncpp
# meson
# ninja
# python-beaker
# python-mako
# python-markupsafe
# rhash
